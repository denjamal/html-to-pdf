﻿using PuppeteerSharp;
using PuppeteerSharp.Media;
using System;
using System.Threading.Tasks;

namespace puppiter
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true
            });
            var page = await browser.NewPageAsync();
            await page.GoToAsync(@"C:\Users\Denis\Desktop\index.html");
            await page.PdfAsync(@"C:\Users\Denis\Desktop\p.pdf", new PdfOptions
            {
                Format = PaperFormat.A4,
                DisplayHeaderFooter = false
            });
        }
    }
}
